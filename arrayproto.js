var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

function Student(name, point) {
	this.name  = name;
	this.point = point;
}
Student.prototype.show = function () {
	console.log('Студент %s набрал %s баллов', this.name, this.point);
};

function StudentList(group, spArray) {
	this.group = group;
	if (spArray){
		for (var s = 0; s < spArray.length; s+=2) {
			this.add( studentsAndPoints[s], studentsAndPoints[s+1] );
		}
	}
}

StudentList.prototype = Object.create(Array.prototype);

StudentList.prototype.add = function (curName, curPoint) {
	this.push(new Student(curName, curPoint));
};

var hj2 = new StudentList('HJ-2', studentsAndPoints);
hj2.add('Татьяна Иванова',10);
hj2.add('Сергей Петров',20);
hj2.add('Юлия Алексеева',10);
hj2.add('Геннадий Дмитриев',20);
hj2.add('Дмитрий Егоров',20);

var html7 = new StudentList('HTML-7');
html7.add('Игорь Борисов', 0);
html7.add('Ирина Сидорова', 0);
html7.add('Кирилл Григорьев', 0);
html7.add('Леонид Сергеев', 0);
html7.add('Зинаида Жоричева',20);

StudentList.prototype.show = function () {
	console.log( "Группа %s (%s студентов):", this.group, this.length);
	this.forEach( function(student) { student.show(); });
};

StudentList.prototype.outStudent = function (who) {
	var studentOut = this.findIndex( function(thisStudent, index){
		if (thisStudent.name == who) {
			return (index);
		}
	});
	return studentOut;
};

var studentForMove = hj2.splice(hj2.outStudent('Татьяна Иванова'), 1);
html7.add(studentForMove[0].name, studentForMove[0].point);

hj2.show();
html7.show();

Student.prototype.valueOf = function() { return this.point; };

StudentList.prototype.max = function () {
	var myMax = Math.max.apply(null, this);
	var myMaxIdx = this.findIndex( function(std){
		if (std == myMax) {	return (std); }
	});
	console.log( 'Максимальный балл-'+myMax+' в группе '+this.group+' набрал студент: '+this[myMaxIdx].name );
};

hj2.max();
html7.max();
